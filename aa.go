package main

import "fmt"

func yourHobbies(name string, hobbies ...string) {
	fmt.Println("halo nama saya adalah", name)
	fmt.Println("dan buah favorit saya adalah:")
	for _, hobby := range hobbies {
		fmt.Println(hobby)
	}
} //kesalahannya 1 kurang tanda } di akhir sebagai penutup func yourHobbies()

func main() {
	yourHobbies("John", "semangka", "jeruk", "melon", "pepaya")
	fmt.Println()

	var hobbies = []string{"cabai", "bawang"}
	yourHobbies("doe", hobbies...) // kesalahan ke 2 seharusnya tidak menggunakan tanda = u medeklarasikan func yourHobbies

}
